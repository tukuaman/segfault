/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	* Implements the Processing Element of the Systolic Array
//	* Inputs: a,b,c_in
//  * Outputs: c_out = cin + a*b;
//	* Implemented using combinational logic
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

package mac;

interface MAC_ifc;
	method int ans(int a, int b, int c_in);
endinterface

(* synthesize *)
module mkMAC(MAC_ifc);
	method int ans(int a, int b, int c_in);
		return c_in + a * b;
	endmethod
endmodule

endpackage: mac
