/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	* Implements the hardware interconnections of systolic architecture with projection vector [1 0 1]
//	* A.B is treated as A[B1, B2, .. BN] = [AB1,AB2,...ABN] and does parallel matrix-vector multiplication
//	* B and C propogate linearly along the Bidirectional Linear Systolic Array in opposite directions
//	* A propogates normal to B and C's direction
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

package arch_4;

import RegFile :: * ;
import Vector :: *;
import get_input :: *;
import mac :: *;

`define K 5
`define NUM_INP 4

interface Arch_4_ifc;
  method Action put_a(Vector#(`K,int) a_in);
  method Action put_b(Vector#(`K,int) b_in);
  method Vector#(`K, int) get_z ();
endinterface

(* synthesize *)
module mkArch_4(Arch_4_ifc);
	
	Vector#(`K,Reg#(int)) a <- replicateM(mkReg(0));
	Vector#(`K,Reg#(int)) z <- replicateM(mkReg(0));

	Reg#(Bool) a_done <- mkReg(False);
	Reg#(Bool) b_done <- mkReg(False);
	
	Vector#(`K,Vector#(`K,MAC_ifc)) macs <- replicateM(replicateM(mkMAC));
	Vector#(`K,Vector#(TSub#(`K,1),Reg#(int))) c_reg <- replicateM(replicateM(mkReg(0)));
	Vector#(`K,Vector#(`K,Reg#(int))) b_reg <- replicateM(replicateM(mkReg(0)));


	rule proc (a_done == True && b_done == True);
		for(Integer i=0; i< `K; i = i+1)
		begin
			for(Integer j = 0; j < `K ; j = j + 1)
			begin
				if(j == 0)
				begin
					c_reg[i][0] <= macs[i][0].ans(a[0],b_reg[i][0],0);
				end
				else if (j!=`K-1)
				begin
					c_reg[i][j] <= macs[i][j].ans(a[j],b_reg[i][j],c_reg[i][j-1]);
				end
				else
				begin
					z[i] <= macs[i][j].ans(a[j],b_reg[i][j],c_reg[i][j-1]);
				end
				if(j != 0)
					b_reg[i][j-1] <= b_reg[i][j];
			end
		end
		a_done <= False;
		b_done <= False;
	endrule

	method Action put_a(Vector#(`K,int) a_in) if(a_done == False);
		for(Integer i = 0; i < `K; i=i+1)
			a[i] <= a_in[i];
		a_done <= True;
	endmethod
	
	method Action put_b(Vector#(`K,int) b_in) if(b_done == False);
		for(Integer i = 0; i < `K; i=i+1)
			b_reg[i][`K-1] <= b_in[i];
		b_done <= True;
	endmethod


	method 	Vector#(`K, int) get_z () if(a_done == False && b_done == False);
		Vector#(`K, int) zz; 
		for(Integer i = 0; i<`K; i = i+1)
			zz[i] = z[i];
		return zz;
	endmethod
endmodule

endpackage: arch_4

