/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	* Implements a Top module that drives the systolic array
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

import RegFile :: * ;
import Vector :: *;
import get_input :: *;
import arch_4 :: *;
import mac :: *;

`define K 5
`define NUM_INP 4

module mkTop(Empty);

	int deep = (3*`K - 2) * `NUM_INP;
	Reg#(Bool) inp_rdy <- mkReg(False);
	Reg#(int) ctr <- mkReg(0);

	REG_ifc mem <- mkREG;
	Arch_4_ifc arr <- mkArch_4;
	
	let fh <- mkReg(InvalidFile) ;
	let fmcd <- mkReg(InvalidFile) ;
	Reg#(int) outCount <- mkReg(0);
	
	rule open_outfile (outCount == 0) ;
		// Open the file and check for proper opening
		// Using a multi-channel descriptor.
		String dumpFile = "temp_results.txt" ;
		File lmcd <- $fopen( dumpFile ) ;
		if ( lmcd == InvalidFile )
		begin
			$display("cannot open %s", dumpFile );
			$finish(0);
		end
		lmcd = lmcd | stdout_mcd ;
		outCount <= 1;
		fmcd <= lmcd ;
	endrule

	rule in_stat(inp_rdy == False);
		inp_rdy <= mem.is_done;
	endrule

	rule drive_in(inp_rdy == True && ctr < deep);
		arr.put_a(mem.get_aT(ctr));
		arr.put_b(mem.get_bT(ctr));
		ctr <= ctr + 1;
	endrule

	rule out_disp(ctr < deep && inp_rdy == True);
		Vector#(`K, int) zz  = arr.get_z();
		for(Integer i = 0; i < `K; i = i + 1)
		begin
			$display("%d", zz[i]);
			$fwrite( fmcd , "%d\n", zz[i]);
		end
	endrule
	
	rule fin(ctr >= deep && inp_rdy == True);
		Vector#(`K, int) zz  = arr.get_z();
		for(Integer i = 0; i < `K; i = i+1)
		begin
			$display("%d", zz[i]);
			$fwrite( fmcd , "%d\n", zz[i]);
		end
		$fwrite( fmcd , "\n K = %d \t NUM_INP = %d\n", `K, `NUM_INP);
		$finish(0);
	endrule	

endmodule  


