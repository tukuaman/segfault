// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes the main module of Architecture 2 as specified in report. 
// It uses K number of MACs to compute out = A.B using repeated MAC operations on these MAC units
// It generates each row of output simulataneously in K steps using the K MAC arrays and iterates this over every row till all matrix multiplications are completed.

// Importing libraries and packages
import RegFile :: * ;
import Vector :: *;
import get_input :: *;
import mac :: *;

// Defining macros
`define K 5										// K is the dimension of the input square matrices
`define NUM_INP 4								// NUM_INP is the number of input matrices to multiply
												// i.e. out1 = A1.B1, out2 = A2.B2, ..., outNUM_INP = ANUM_INP.BNUM_INP

// This the main module. It constructs the MAC array, take input from get_input module,
// Performs proper interfacing of inputs to each MAC and collects the output using repeated MAC operations.
(* synthesize *)
  module mkArch_2 (Empty);

	// Defining variables used
	int grid_SQ = `K*`K;	 								// K * K
	int max_MAC_ITR = `K + 1;								// Total number of MAC steps to get every single row of output
	int max_ITR = max_MAC_ITR * `K * `NUM_INP;				// Total number of MAC steps for all matrix multiplications to complete on a particular MAC

	// Declaring flags and counters
	Reg#(Bool) inpDone <- mkReg(False);			// Flag to indicate that input matrix is ready to be taken from get_input module
	Reg#(Bool) macDone <- mkReg(False);			// Flag to indicate that a single MAC operation on a MAC is done
	Reg#(Bool) putDone <- mkReg(False);			// Flag to indicate that new values are given to the MACs
	Reg#(int)  macNum  <- mkReg(0);				// Counter to indicate the state of MACs during a particular matrix multiplication 
	Reg#(int)  itrNum  <- mkReg(0);				// Counter to indicate the total number of MAC operations performed on a particular MAC
	Reg#(int)  rowNum  <- mkReg(0);				// Counter to indicate the current row number of output
	Reg#(int)  colNum  <- mkReg(0);				// Counter to indicate the current column number of output
	Reg#(int)  matNum  <- mkReg(0);				// Counter to indicate the current matrix number of output

	// Instantiating modules
	REG_ifc regTop <- mkREG;															// Instantiating the regTop module specified in get_input
	Vector#(`K, MAC_ifc)   mac     <- replicateM(mkMAC);								// Creating the K MAC array
	Vector#(`K, Reg#(int)) x       <- replicateM(mkReg(0));								// Creating the K "x" input registers of the MAC array
	Vector#(`K, Reg#(int)) y       <- replicateM(mkReg(0));								// Creating the K "y" input registers of the MAC array
	Vector#(TMul#(TMul#(`K,`K), `NUM_INP), Reg#(int)) out <- replicateM(mkReg(0));		// Creating the vector to store output

	// Following code used for opening the result file that will show the final outputs
	// This part is directly taken from BSV reference guide
	let fh <- mkReg(InvalidFile) ;
	let fmcd <- mkReg(InvalidFile) ;
	Reg#(int) outCount <- mkReg(0);
	
	// Rule to open the output file
	rule open_outfile (outCount == 0) ;
		// Open the file and check for proper opening
		// Using a multi-channel descriptor.
		String dumpFile = "results.txt" ;
		File lmcd <- $fopen( dumpFile ) ;
		if ( lmcd == InvalidFile )
		begin
			$display("cannot open %s", dumpFile );
			$finish(0);
		end

		lmcd = lmcd | stdout_mcd ;
		outCount <= 1;
		fmcd <= lmcd ;
	endrule

	// Rule to start and set input matrices as ready
	rule set_input (!inpDone);
		let done = regTop.is_done();						// is_done() from get_input file tells if pre-processing done or not
		if (done)											// If pre-processing is done, set inpDone and macDone for MAC operations to start
		begin 
			inpDone <= True;
			macDone <= True;
		end
	endrule

	// Rule to start the MAC units from mac file and set the number of operations at a MAC before MAC units get reset
	rule mac_start;
		// Iterating over all MAC units
		for (int i = 0; i < `K; i = i + 1)
		begin
			mac[i].put_start ();
			mac[i].put_numItr(max_MAC_ITR);
		end
	endrule
    
	// descending urgency needed since both these rules can trigger at same time but preference should be given to mac_reset if can fire
	(* descending_urgency = "mac_reset, mac_fsm" *)

	// This rule resets all MAC units after every max_MAC_ITR steps to allow for next row processing
	rule mac_reset (inpDone && macDone && macNum == max_MAC_ITR);
		// Iterating over all MAC units
		for (int i = 0; i < `K; i = i + 1)
			mac[i].put_reset ();

		macNum <= 0;							// Setting the state of MACs to initial default state
		itrNum <= itrNum - 1;					// Needed since number of MAC steps is (K + 1) to allow for reset, while dimension of input matrices is K x K 
	endrule

	// This rule controls the flow of data to different MAC units based on the state of the MACs and counters
	rule mac_fsm (inpDone && macDone);
	
		if (macNum < max_MAC_ITR - 1)								// i.e. still a particular row product is not ready
		begin
			let aT = regTop.get_aT(macNum, rowNum, matNum);			// aT carries the appropriate element of the row, and is given to the MAC array as "x" input
			let bT = regTop.get_bT(macNum, matNum);					// bT carries the appropriate row vector of size K that is given to the MAC array as "y" input

			// Iterating over all MAC units
			for (int i = 0; i < `K; i = i + 1)
			begin
				x[i] <= aT;											// Setting the "x" input of the MAC array 
				y[i] <= bT[i];										// Setting the "y" input of the MAC array
			end
		end

		else														// i.e. reached the last step of a output row generation state
		begin
			// Iterating over all MAC units
			for (int i = 0; i < `K; i = i + 1)
			begin
				x[i] <= 0;											// Setting all inputs of MACs as zero, 
				y[i] <= 0;											// since it is the last stage and is just waiting for last computation to complete at the MACs
			end
		end

		itrNum <= itrNum + 1;										// Incrementing the total iteration counter
		macNum <= macNum + 1;										// Incrementing the state of the MACs
		macDone <= False;											// Setting macDone flag as false since new computation still not complete on the MACs
		putDone <= False;											// Setting putDone flag as false since new data has not been given to the MACs
	endrule

	// This rule puts the "x" & "y" data to the MACs 
    rule mac_put (inpDone && !macDone && !putDone);
		// Iterating over all MAC units
		for (int i = 0; i < `K; i = i + 1)
		begin
			mac[i].put_x (x[i]);									// Giving the "x" input to the MAC
			mac[i].put_y (y[i]);									// Giving the "y" input to the MAC
		end
		putDone <= True;
    endrule

	// This rule allows for checking if an output row is ready, and printing it in output file, and then iterating over for other rows
    rule mac_work (inpDone && !macDone && putDone);

		if(itrNum == max_ITR)										// i.e. reached end of all computations, so end of program
			$finish (0);

		if(macNum == max_MAC_ITR)									// i.e. reached last stage of a particular output row generation
		begin
			// Iterating over all MAC units
			for (int i = 0; i < `K; i = i + 1)
			begin
				let z = mac[i].get_out ();							// Querying get_out from the MAC unit which will fire only when a particular MAC operation is done
				out[(rowNum * `K + i) + grid_SQ * matNum] <= z;		// Collecting the output in out matrix
				$fwrite( fmcd , "%d\t", z);							// Writing the output to the output file
			end

			if(rowNum == `K - 1)									// Updating the row number and matrix number
			begin
				rowNum <= 0;
				matNum <= matNum + 1;			
				$fwrite( fmcd , "\n\n");
			end
			else
			begin													// Incrementing the row number
				rowNum <= rowNum + 1;
				$fwrite( fmcd , "\n");
			end
		end

		macDone <= True;											// Setting macDone flag since a particular MAC operation is done now 
    endrule

  endmodule: mkArch_2

