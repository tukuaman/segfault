// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes the input extraction module of Architecture 2. 
// It reads input matrices into a RegFile and directly accesses correct sequence to be given to MAC array.

// Defining package get_input
package get_input;

// Importing libraries and packages
import RegFile :: * ;
import Vector :: *;

// Defining macros
`define K 5										// K is the dimension of the input square matrices
`define NUM_INP 4								// NUM_INP is the number of input matrices to multiply
												// i.e. out1 = A1.B1, out2 = A2.B2, ..., outNUM_INP = ANUM_INP.BNUM_INP

// Declaring the interface
interface REG_ifc;
  method int get_aT (int k, int i, int n);					// To get the a[i][k] element to be given as "x" input to MAC array at state k
  method Vector#(`K, int) get_bT (int k, int n);			// To get the b[k] row vector to be given as "y" input to MAC array at state k
  method Bool is_done();									// To query if pre-processing is done
endinterface: REG_ifc

// Defining the module mkREG
(* synthesize *)
  module mkREG (REG_ifc);

	int grid_SQ = `K*`K;	 											// K * K, total number of elements in a matrix
	int max_ADRS = `NUM_INP * grid_SQ - 1;								// Maximum address of input register files

	Reg#(Bool) preDone <- mkReg(False);									// Flag to indicate that pre-processing is done
	Reg#(int)  preNum  <- mkReg(0);										// Counter to indicate the current row number of input matrices

	RegFile#(int,int) regA <- mkRegFileLoad("a.txt",0, max_ADRS); 		// RegFile that reads input A matrices
	RegFile#(int,int) regB <- mkRegFileLoad("b.txt",0, max_ADRS);		// RegFile that reads input B matrices
	Vector#(TAdd#(TMul#(TMul#(`K,`K), `NUM_INP), `K), Reg#(int)) vecB <- replicateM(mkReg(0));		// Creating the vector to store B matrices

	// Copying B matrices in a vector
	rule store_B (!preDone);
		if(preNum <= max_ADRS)
		begin
			vecB[preNum] <= regB.sub(preNum);
		end
		else				
		begin
			preDone <= True;
		end
		preNum <= preNum + 1;
	endrule

	// Method to query if pre-processing is done
	method Bool is_done if (preDone);
		return True;
	endmethod

	// Method to get the a[i][k] element to be given as "x" input to MAC array at state k
	method int get_aT (int k, int i, int n);
		return regA.sub(`K * i + k + n * grid_SQ);
	endmethod

	// Method to get the b[k] row vector to be given as "y" input to MAC array at state k
	method Vector#(`K, int) get_bT (int k, int n);
		Vector#(`K, int) bT; 
		for (int t = 0; t < `K; t = t + 1) 
			bT[t] = vecB[`K * k + t + n * grid_SQ];
		return bT;
	endmethod

  endmodule: mkREG

endpackage: get_input
