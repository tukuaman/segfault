// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes a single MAC unit.
// It performs sequential multiplication using shift and add operations, instead of a combinational logic. 
// product (accumulator) = previous vlaue + x.y

// Defining package mac
package mac;

// Declaring the interface for mac
interface MAC_ifc;
  method Action put_start ();								// Method to allow for mac to get started
  method Action put_reset ();								// Method to reset the accumulated product and number of iterations to default zero
  method Action put_x (int x);								// Used to give "x" input to the mac
  method Action put_y (int y);								// Used to give "y" input to the mac
  method Action put_numItr(int z);							// Used to set total number of iteration on the mac						
  method int get_out ();									// Used to query the accumulator output of the mac
  method Bool is_done();									// Used to query if a particular operation on mac is done or not
endinterface: MAC_ifc

// Defining mac module
(* synthesize *)
  module mkMAC (MAC_ifc);
	
    Reg#(int)  product    <- mkReg (0);						// Register to accumulate value on the mac
    Reg#(int)  d          <- mkReg (0);						// Used to perform binary multiplication
    Reg#(int)  r          <- mkReg (0);						// Used to perform binary multiplication
    Reg#(int)  iterations <- mkReg (0);						// Defines the total number of iterations of the mac 

	// Declaring flags and counters
    Reg#(Bool) start      <- mkReg (False);					// Flag to indicate if mac unit is ON (start) or not
    Reg#(Bool) got_x      <- mkReg (False);					// Flag to indicate if got the new x input
    Reg#(Bool) got_y      <- mkReg (False);					// Flag to indicate if got the new y input
    Reg#(Bool) done       <- mkReg (False);					// Flag to indicate if a particular operation of multiply and accumulate is done or not
    Reg#(int)  count      <- mkReg (0);						// Counter to indicate the iteration state of the mac
    
	// This rule performs the binary multiply and accumulate operation on the mac
	rule mac_compute (got_x && got_y && start) ;

		if (pack(r)[0] == 1) 
			product <= product + d;
		d <= d << 1;
		r <= r >> 1;

		// Reached end of operation, set appropriate flags
		if (r == 0)
		begin
			got_x <= False;
			got_y <= False;
			done <= True;
		end
		else
			done <= False;
	endrule

	// Method to allow for mac to get started
	method Action put_start () if (! start);
		start <= True;
		product <= 0;										// Set accumulator to zero
	endmethod

	// Method to set total number of iteration on the mac	
	method Action put_numItr (int z);
	    iterations <= z;
	endmethod

	// Method to reset the accumulated product and number of iterations to default zero
	method Action put_reset () if (start);
		product <= 0;
		count <= 0;
	endmethod

	// Used to query if a particular operation on mac is done or not
	method Bool is_done ();
		return done;
	endmethod

	// Used to give "x" input to the mac
	method Action put_x (int x) if ((! got_x) && start && (count < iterations));
		d <= x; 											
		got_x <= True; 	
		count <= count + 1;									// incrementing count
	endmethod

	// Used to give "y" input to the mac
	method Action put_y (int y) if ((! got_y) && start);
		r <= y; 
		got_y <= True;
	endmethod

	// Used to query the accumulator output of the mac
	method int get_out if (done);
		return product;
	endmethod

	endmodule: mkMAC

endpackage: mac
