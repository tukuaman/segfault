// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes the input extraction module of Architecture 3. 
// It reads input matrices into a RegFile and directly accesses correct sequence to be given to MAC.

// Defining package get_input
package get_input;

// Importing libraries and packages
import RegFile :: * ;
import Vector :: *;

// Defining macros
`define K 5										// K is the dimension of the input square matrices
`define NUM_INP 4								// NUM_INP is the number of input matrices to multiply
												// i.e. out1 = A1.B1, out2 = A2.B2, ..., outNUM_INP = ANUM_INP.BNUM_INP

// Declaring the interface
interface REG_ifc;
  method int get_aT (int k, int i, int j, int n);						// To get the a[i][k] element to be given as "x" input to MAC at state k
  method int get_bT (int k, int i, int j, int n);						// To get the b[k][j] element to be given as "y" input to MAC at state k
endinterface: REG_ifc

// Defining the module mkREG
(* synthesize *)
  module mkREG (REG_ifc);

	int grid_SQ = `K * `K;	 											// K * K, total number of elements in a matrix
	int max_ADRS = `NUM_INP * grid_SQ - 1;								// Maximum address of input register files
	
	RegFile#(int,int) vecA <- mkRegFileLoad("a.txt",0, max_ADRS); 		// RegFile that reads input A matrices
	RegFile#(int,int) vecB <- mkRegFileLoad("b.txt",0, max_ADRS);		// RegFile that reads input B matrices

  // Method to get the a[i][k] element to be given as "x" input to MAC at state k
  method int get_aT (int k, int i, int j, int n);
  		return vecA.sub(`K * i + k + n * grid_SQ);
	endmethod

  // Method to get the b[k][j] element to be given as "y" input to MAC at state k
  method int get_bT (int k, int i, int j, int n);
  		return vecB.sub(`K * k + j + n * grid_SQ);
	endmethod

  endmodule: mkREG

endpackage: get_input
