/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	* Implements scheduling of inputs A and B that goes into Systolic Array
//	* Scheduled A and B as stored in instances of this module
//	* Simple minded implementation of algo descibed in:
// https://www.google.co.in/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&sqi=2&ved=0CCYQFjABahUKEwizyIyw39rIAhVGQI4KHfEiB4U&url=http%3A%2F%2Fwww.np.ac.rs%2Fen%2Fpreuzimanjasve%2Fpublications%2Fvol2br1%2F184-matrix-multiplication-on-linear-bidirectional-systolic-arrays%2Fdownload&usg=AFQjCNFVvFA1YqFRyVzoYlIzpHcRCzqyEg&sig2=jYWKFbKF5mDQPP5ncB1wMg&bvm=bv.105841590,d.c2E&cad=rja
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

package get_input;

import RegFile :: * ;
import Vector :: *;

`define K 5
`define NUM_INP 4

typedef struct {Integer x; Integer y;} Cord;

interface REG_ifc;
  method Vector#(`K, int) get_aT (int tt);					// outputs one row of scheduled A values
  method Vector#(`K, int) get_bT (int tt);					// outputs one col of schedule B values
  method Bool is_done();									// flags when scheduling is completed
endinterface: REG_ifc

module mkREG(REG_ifc);
	
	int grid_SQ = `K * `K;									// number of entries in K by K matrix
	int max_STEPS = 3 * `K - 2;								// total number of steps required for a matrix multiplication
	int max_ADRS = grid_SQ * `NUM_INP - 1;					// hi address of register file
	
	Reg#(Bool) en <- mkReg(False);	
	Reg#(int) ctr_i <- mkReg(0);
	Reg#(int) ctr_j <- mkReg(0);
	Reg#(Bool) init <- mkReg(False);
	Reg#(int) ctr_a <- mkReg(0);
	Reg#(int) ctr_y <- mkReg(0);
	Reg#(Bool) d_b <- mkReg(True);

	Vector#(`NUM_INP,Vector#(100,Reg#(int))) gen <- replicateM(replicateM(mkReg(-1)));
	Vector#(`NUM_INP,Reg#(int)) lens <- replicateM(mkReg(0));
	RegFile#(int,int) vecA <- mkRegFileLoad("a.txt",0, max_ADRS);
	RegFile#(int,int) vecB <- mkRegFileLoad("b.txt",0, max_ADRS);
	
	// aT - stores scheduled A
	// bT - stores scheduled B
	Vector#(TAdd#(TMul#(TSub#(TMul#(3, `K), 2), `NUM_INP),TSub#(`K,1)), Vector#(`K,Reg#(int))) aT<- replicateM(replicateM(mkReg(0)));
	Vector#(`K,Vector#(TMul#(TSub#(TMul#(3, `K), 2), `NUM_INP),Reg#(int))) bT <- replicateM(replicateM(mkReg(0)));

	Reg#(int) m <- mkReg(-1);
	Reg#(int) v <- mkReg(-1);
    Reg#(int) rowNum <- mkReg(0);
    Reg#(int) colNum <- mkReg(0);
    Reg#(int) matNum <- mkReg(0);

	Reg#(Bool) flag <- mkReg(False);
	Reg#(int) bctr <- mkReg(0);
	Reg#(int) temp <- mkReg(0);
	Reg#(int) colNum2 <- mkReg(0);
	Reg#(int) rowNum2 <- mkReg(0);
	Reg#(int) matNum2 <- mkReg(0);

	Integer n = `K;
	Integer l_L[`K]; 
	Integer l[`K];
	Cord t[`K][100];
	
	Integer  pos[`K][100];									// stores scheduled positions of a col of B
	Cord	 a[`K][`K];										// stores scheduled positions for entire A
	for (Integer ii=0; ii< `K; ii=ii+1)
		for (Integer jj=0; jj < 100; jj=jj+1)
			pos[ii][jj] = -1;
	
	Integer i,j,k;

/*********************************************************************	
/*	- solving matrix equations for scheduling one col vector of B
/*	- refer to paper for details
/********************************************************************/	
	for(j=1;j<=n;j = j+1)
	begin
		Integer len = 0;
		for(i=1;i<=n;i=i+1)
		begin
			for(k=1;k<=n;k=k+1)
			begin
				if(i+k == j+1 || i+k == j+1+n)
				begin
					Integer y;
					Bool add = True;
					for(y=0;y<len;y = y+1)
					begin
						if(t[j-1][y].x == i && t[j-1][y].y == k && add)
							begin
								 add = False;
							end
					end
					if(add)
					begin
						Cord foo;foo.x = i; foo.y =k;
						t[j-1][len] = foo;
						len = len + 1;
					end
				end
			end
		end
		l_L[j-1] = len;
	end	
	
	for(i=0;i<n;i = i+1)
	begin
		Integer len = 0;
		//cout<<"i = "<<i+1<<":"<<endl;
		for(j=0;j<l_L[i];j=j+1)
		begin
			//cout<<t[i][j].x<<" "<<t[i][j].y<<endl;
			Integer p,q,n_cap;
			if(n%2 == 1) n_cap = n;
			else n_cap = n-1;
			if(n_cap < 2*(t[i][j].x - 1))
				p = 2*(t[i][j].x+t[i][j].y)-3-n_cap;
			else
				p = 2*(t[i][j].x+t[i][j].y)-3;
				
			Bool add = True;
			for(q=0;q<len;q=q+1)
			begin
				if(pos[i][q] == p && add)
				begin
				 add = False; 
				end
			end
			if(add)
			begin
				pos[i][len] = p;
				len = len + 1;
			end
		end
		l[i] = len;
	end


/*********************************************************************	
/*	- solving matrix equations for scheduling entire A matrix
/*	- refer to paper for details
/********************************************************************/	

	for(i=0;i<n;i=i+1)
	begin
		for(j=0;j<n;j=j+1)
		begin
			k = j+1-i;
			if(k<=0) k = k+n;
			
			Integer n_cap;
			if(n%2 == 1) n_cap = n;
			else n_cap = n-1;
			
			if(n_cap < 2*i)
			begin
				Cord foo;
				foo.x = k-1;
				foo.y = -3+(2*i+2)+k-n_cap;
				a[i][j] = foo;
			end
			else
			begin
				Cord foo;
				foo.x = k-1;
				foo.y = -3+(2*i+2)+k;
				a[i][j] = foo;
			end
		end
	end
	

/*********************************************************************	
/*	- initiates scheduling
/********************************************************************/	
	rule start(!init);
		init <= True;
	endrule
			
/*********************************************************************	
/*	- for debug purposes
/********************************************************************/	
	rule display_b(ctr_i < `K && init);
		if(ctr_j == fromInteger(l[ctr_i]))
		begin
			ctr_j <= 0;
			ctr_i <= ctr_i + 1;
		end
		else
		begin
			ctr_j <= ctr_j + 1;
		end
	endrule

/*********************************************************************	
/*	- for debug purposes
/********************************************************************/	
	rule set(ctr_i == `K && init && d_b);
		d_b <= False;
	endrule

/*********************************************************************	
/*	- for debug purposes
/********************************************************************/	
	rule display_a(ctr_a< `K && !d_b && init);
		if(ctr_y == `K)
		begin
			ctr_y <=0;
			ctr_a <= ctr_a + 1;
		end
		else
		begin
			ctr_y <= ctr_y + 1;
		end
	endrule
	
/************************************************************************	
/*	- Initialises input A for the array according to schedule calculated
/***********************************************************************/	
	rule init_A(ctr_i == `K && init && !d_b && ctr_a == `K && matNum < `NUM_INP);
			aT[fromInteger(a[rowNum][colNum].y) + matNum * max_STEPS+`K-fromInteger(1)][a[rowNum][colNum].x] <= vecA.sub(rowNum*`K + colNum + matNum*grid_SQ);
			if(colNum == `K - 1 && rowNum == `K - 1)
			begin
				rowNum <= 0;
				colNum <= 0;
				matNum <= matNum + 1;			
			end
			else if(colNum == `K - 1)
			begin
				rowNum <= rowNum + 1;
				colNum <= 0;			
			end
			else
			begin
				colNum <= colNum + 1;
			end
	endrule
	
	
/************************************************************************	
/*	- Reads in B one value at a time
/***********************************************************************/	
	rule init_B(flag == False && ctr_i == `K && init && !d_b && ctr_a == `K && matNum2 < `NUM_INP);
		temp <= vecB.sub(rowNum2*`K + colNum2 + matNum2*grid_SQ);
		flag <= True;
	endrule
	
/*********************************************************************************
/*	- Fills in the read value of B from previous rule to appropriate positions in
	  the matrix that is fed as B input to the array
/********************************************************************************/	
	rule proc_iter(flag == True && ctr_i == `K && init && !d_b && ctr_a == `K);
		if(bctr < fromInteger(l[rowNum2]))
		begin
			bT[colNum2][fromInteger(pos[rowNum2][bctr]-1)+ matNum2 * max_STEPS] <= temp;
			bctr <= bctr + 1;
		end
		else
		begin
			bctr <= 0;
			flag <= False;
			if(colNum2 == `K - 1 && rowNum2 == `K - 1)
			begin
				rowNum2 <= 0;
				colNum2 <= 0;
				matNum2 <= matNum2 + 1;			
			end
			else if(colNum2 == `K - 1)
			begin
				rowNum2 <= rowNum2 + 1;
				colNum2 <= 0;			
			end
			else
			begin
				colNum2 <= colNum2 + 1;
			end
		end	
	endrule

/************************************************************************	
/*	- Flags true when all scheduling is done
/***********************************************************************/	
	method Bool is_done if (matNum == `NUM_INP && matNum2 == `NUM_INP);
		return True;
	endmethod

/************************************************************************	
/*	- Returns one row of scheduled A when requested
/***********************************************************************/	
	method Vector#(`K, int) get_aT (int tt);
		Vector#(`K, int) aK; 
		for (Integer i = 0; i < `K; i = i + 1) 
			aK[i] = aT[tt][i];
		return aK;
	endmethod

/************************************************************************	
/*	- Returns one col of scheduled B when requested
/***********************************************************************/	
	method Vector#(`K, int) get_bT (int tt);
		Vector#(`K, int) bK; 
		for (Integer i = 0; i < `K; i = i + 1) 
			bK[i] = bT[i][tt];
		return bK;
	endmethod
endmodule

endpackage: get_input
