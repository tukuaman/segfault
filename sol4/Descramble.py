from numpy import *

with open('temp_results.txt') as f:
    content = f.readlines()


string = content[-1]

print string

gh = [int(s) for s in string.split() if s.isdigit()]
n = gh[0]
num = gh[1]
ctr = 0
print gh

f = open('dump.txt','w')

for i in content[:-2]:
	f.write(i)
f.close()

data = loadtxt('dump.txt')

init = n*(2*n-1)
bw = 2*n*(n-1)

n_cap = 0
l = {}
if n%2 == 0:
	n_cap = n - 1
else:
	n_cap = n
c = range(n)
for i in range(1,n+1):
	if n_cap < 2*(i-1):
		l[(-1)*(1-2*i+n_cap)] = i-1;
	else:
		l[(-1)*(1-2*i)] = i - 1;

f = open('results.txt','w')

data = data[init:]
x = [[-1 for i in range(10)] for j in range(10)]
x = array(x)
while ctr < num:
	ctr = ctr + 1
	t = data[:n]
	for i in range(n*n):
		#print type(data[i])
		#print type(c[0][0])
		x[l[(i/n)+1]][i%n] = data[i]
	data = data[n*n+2*n*(n-1):]
	for i in range(n):
		for j in range(n):
			f.write("\t\t"+str(x[i][j]));
		f.write("\n")
	f.write("\n")

