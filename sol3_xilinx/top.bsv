// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes the top module of Architecture 3.
// It simply signals the computations to start and queries for the output. 

package top;

// Importing libraries and packages
import arch_3::*;
import RegFile :: * ;
import Vector :: *;

// Defining macros
`define K 5										// K is the dimension of the input square matrices
`define NUM_INP 4								// NUM_INP is the number of input matrices to multiply
												// i.e. out1 = A1.B1, out2 = A2.B2, ..., outNUM_INP = ANUM_INP.BNUM_INP

module mkTop (Empty);

	Arch_3_ifc ifc <- mkArch_3;					// Instantiating the architecture
	Vector#(TMul#(TMul#(`K,`K), `NUM_INP), Reg#(int)) out <- replicateM(mkReg(0));			// Output vector to store outputs

	Reg#(Bool) go     <- mkReg(True);				// To signal start of computation
	Reg#(int) counter <- mkReg(0);					
	Reg#(Bool) done   <- mkReg(False);				// To signal completion of computation
	int _CEIL = `K * `K * `NUM_INP - 1; 			// Total number of output elements
	
	// Rule to start the computation
	rule putstart(go);
		ifc.put_start(go);
		go <= False;
	endrule:putstart
	
	// Rule to collect the ouputs
	rule collect(!go && !done);
		out[counter] <= ifc.get_out(counter);
		if(counter == _CEIL)
		   done <= True;
		counter <= counter + 1; 
	endrule:collect
	
endmodule :mkTop
endpackage: top
