// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes the main module of Architecture 3 as specified in report. 
// It uses single MAC unit to compute out = A.B using repeated MAC operations
// It generates each element of output sequentially in K steps and similarly computes all K * K output matrix elements till all matrix multiplications are completed.

package arch_3;

// Importing libraries and packages
import RegFile :: * ;
import Vector :: *;
import get_input :: *;
import mac :: *;

// Defining macros
`define K 5										// K is the dimension of the input square matrices
`define NUM_INP 4								// NUM_INP is the number of input matrices to multiply
												// i.e. out1 = A1.B1, out2 = A2.B2, ..., outNUM_INP = ANUM_INP.BNUM_INP

// Declaring interface for Arch_3 to interact with top
interface Arch_3_ifc;
  method int get_out (int ind);					// To access the output vector
  method Action put_start (Bool go);			// TO signal start of computation
endinterface: Arch_3_ifc

// This the main module. It constructs the single MAC unit, take input from get_input module,
// Performs proper interfacing of inputs to MAC and collects the output using repeated MAC operations.
(* synthesize *)
  module mkArch_3 (Arch_3_ifc);

	// Defining variables used
	int grid_SQ = `K*`K;	 								// K * K
	int max_MAC_ITR = `K + 1;								// Total number of MAC steps to get every single row of output
	int max_ITR = max_MAC_ITR * grid_SQ * `NUM_INP;				// Total number of MAC steps for all matrix multiplications to complete on a particular MAC

	// Declaring flags and counters
	Reg#(Bool) inpDone <- mkReg(False);			// Flag to indicate that input matrix is ready to be taken from get_input module
	Reg#(Bool) macDone <- mkReg(False);			// Flag to indicate that a single MAC operation on a MAC is done
	Reg#(Bool) putDone <- mkReg(False);			// Flag to indicate that new values are given to the MACs
	Reg#(int)  macNum  <- mkReg(0);				// Counter to indicate the state of MACs during a particular matrix multiplication 
	Reg#(int)  itrNum  <- mkReg(0);				// Counter to indicate the total number of MAC operations performed on a particular MAC
	Reg#(int)  rowNum  <- mkReg(0);				// Counter to indicate the current row number of output
	Reg#(int)  colNum  <- mkReg(0);				// Counter to indicate the current column number of output
	Reg#(int)  matNum  <- mkReg(0);				// Counter to indicate the current matrix number of output
	Reg#(Bool) start   <- mkReg(False);

	// Instantiating modules
	REG_ifc regTop 	  <- mkREG;					// Instantiating the regTop module specified in get_input
	MAC_ifc mac 	  <- mkMAC;					// Creating the single MAC unit
	Reg#(int) x       <- mkReg(0);				// Creating the "x" input register of the MAC unit
	Reg#(int) y       <- mkReg(0);				// Creating the "y" input register of the MAC unit

	Vector#(TMul#(TMul#(`K,`K), `NUM_INP), Reg#(int)) out <- replicateM(mkReg(0));				// Creating the vector to store output

	// Following code used for opening the result file that will show the final outputs
	// This part is directly taken from BSV reference guide
	let fh <- mkReg(InvalidFile) ;
	let fmcd <- mkReg(InvalidFile) ;
	Reg#(int) outCount <- mkReg(0);
	
	// Rule to open the output file
	rule open_outfile (outCount == 0 && start) ;
		// Open the file and check for proper opening
		// Using a multi-channel descriptor.
		String dumpFile = "results.txt" ;
		File lmcd <- $fopen( dumpFile ) ;
		if ( lmcd == InvalidFile )
		begin
			$display("cannot open %s", dumpFile );
			$finish(0);
		end
		lmcd = lmcd | stdout_mcd ;
		outCount <= 1;
		fmcd <= lmcd ;
	endrule

	// Rule to start and set input matrices as ready
	rule set_input (!inpDone && start);
		inpDone <= True;
		macDone <= True;
	endrule

	// Rule to start the MAC unit from mac file and set the number of operations at a MAC before MAC unit get reset
	rule mac_start (start);
		mac.put_start ();
		mac.put_numItr(max_MAC_ITR + 1);
	endrule
    
	// descending urgency needed since both these rules can trigger at same time but preference should be given to mac_reset if can fire
	(* descending_urgency = "mac_reset, mac_fsm" *)

	// This rule resets MAC unit after every max_MAC_ITR steps to allow for next element processing
    rule mac_reset (inpDone && macDone && macNum == max_MAC_ITR && start);
		mac.put_reset ();
		macNum <= 0;										// Setting the state of MAC to initial default state
		itrNum <= itrNum - 1;								// Needed since number of MAC steps is (K + 1) to allow for reset, while dimension of input matrices is K x K 
	endrule

	// This rule controls the flow of data to MAC unit based on the state of the MAC and counters
	rule mac_fsm (inpDone && macDone && start);

		if (macNum < max_MAC_ITR - 1)										// i.e. still a particular element is not ready
		begin	
			let aT = regTop.get_aT(macNum, rowNum, colNum, matNum);			// aT carries the a[rowNum][macNum] element, and is given to the MAC as "x" input
			let bT = regTop.get_bT(macNum, rowNum, colNum, matNum);			// bT carries the b[macNum][colNum] element, and is given to the MAC as "y" input
			x <= aT;														// Setting the "x" input of the MAC
			y <= bT;														// Setting the "y" input of the MAC 
		end
		
		else																// i.e. reached the last step of a output element state
		begin
			x <= 0;															// Setting input of MAC as zero, 
			y <= 0;															// since it is the last stage and is just waiting for last computation to complete at the MAC
		end
		
		itrNum <= itrNum + 1;										// Incrementing the total iteration counter
		macNum <= macNum + 1;										// Incrementing the state of the MAC
		macDone <= False;											// Setting macDone flag as false since new computation still not complete on the MAC
		putDone <= False;											// Setting putDone flag as false since new data has not been given to the MAC
	endrule

	// This rule puts the "x" & "y" data to the MACs 
	rule mac_put (inpDone && !macDone && !putDone && start);
		mac.put_x (x);									// Giving the "x" input to the MAC
		mac.put_y (y);									// Giving the "y" input to the MAC
		putDone <= True;
	endrule

	// This rule allows for checking if an output element is ready, and printing it in output file, and then iterating over for other elements
	rule mac_work (inpDone && !macDone && putDone && start);

		if(itrNum == max_ITR)										// i.e. reached end of all computations, so end of program
			$finish (0);
			
		if(macNum == max_MAC_ITR)									// i.e. reached last stage of a particular output element generation
		begin
			let z = mac.get_out ();									// Querying get_out from the MAC unit which will fire only when a particular MAC operation is done
			out[(rowNum * `K + colNum) + grid_SQ * matNum] <= z;	// Collecting the output in out matrix
			$fwrite( fmcd , "%d\t", z);								// Writing the output to the output file

			if(colNum == `K - 1 && rowNum == `K - 1)				// Updating the row number, column number and matrix number
			begin
				rowNum <= 0;
				colNum <= 0;
				matNum <= matNum + 1;			
				$fwrite( fmcd , "\n\n");
			end
			else if(colNum == `K - 1)								// Updating the row number and column number
			begin
				rowNum <= rowNum + 1;
				colNum <= 0;			
				$fwrite( fmcd , "\n");
			end
			else													// Updating the column number
			begin
				colNum <= colNum + 1;
			end
		end
		
		macDone <= True;											// Setting macDone flag since a particular MAC operation is done now 
	endrule

	// Method to get output vector
	method int get_out (int ind);
		return out[ind];
	endmethod
	
	// Method to signal start of computation
	method Action put_start (Bool go);
		start <= go;
	endmethod

  endmodule: mkArch_3
  
endpackage: arch_3
