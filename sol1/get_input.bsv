// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes the pre-processing module of Architecture 1. 
// It reads input matrices into a RegFile and generates the sequence to be given to MAC grid.

// Defining package get_input
package get_input;

// Importing libraries and packages
import RegFile :: * ;
import Vector :: *;

// Defining macros
`define K 5										// K is the dimension of the input square matrices
`define NUM_INP 4								// NUM_INP is the number of input matrices to multiply
												// i.e. out1 = A1.B1, out2 = A2.B2, ..., outNUM_INP = ANUM_INP.BNUM_INP

// Declaring the interface
interface REG_ifc;
  method Vector#(`K, int) get_aT (int t);						// To get the aT vector to be given as "x" input to first column of MAC grid at state t
  method Vector#(`K, int) get_bT (int t);						// To get the bT vector to be given as "y" input to first row of MAC grid at state t
  method Bool is_done();										// To query if pre-processing is done
endinterface: REG_ifc

// Defining the pre-processing module mkREG
(* synthesize *)
module mkREG (REG_ifc);

	int grid_SQ = `K * `K;	 									// K * K, total number of elements in a matrix
	int max_STEPS = `K + `K + `K - 2; 							// 3K -2, this is the dimension of processed input matrices that enters the MAC grid.
	int max_ADRS = `NUM_INP * grid_SQ - 1;						// Maximum address of input register files
	
	RegFile#(int,int) vecA <- mkRegFileLoad("a.txt",0, max_ADRS); 						// RegFile that reads input A matrices
	RegFile#(int,int) vecB <- mkRegFileLoad("b.txt",0, max_ADRS); 						// RegFile that reads input B matrices
	
	Vector#(`K,Vector#(TMul#(TSub#(TMul#(3,`K), 2), `NUM_INP), Reg#(int))) aT <- replicateM(replicateM(mkReg(0)));			// Processed vector aT of K x 3K-2 with additional zeros according to proper sequencing
	Vector#(TMul#(TSub#(TMul#(3,`K), 2), `NUM_INP), Vector#(`K,Reg#(int))) bT <- replicateM(replicateM(mkReg(0)));			// Processed vector bT of 3K-2 x K with additional zeros according to proper sequencing
	
	// Defining variables
	Reg#(int) row_a <- mkReg(0);
	Reg#(int) col_a <- mkReg(0);	
	Reg#(int) row_b <- mkReg(0);
	Reg#(int) col_b <- mkReg(0);	

	Reg#(int) m <- mkReg(0);	
	Reg#(int) v <- mkReg(0);	

/*****************************************************************************************************
/*		- logic for filling aT
/*		- similar to rule where bT is filled
/*		- logic for filling bT is applied in a transposed manner
/*****************************************************************************************************/
	rule init_A (row_a <= `K - 1 && v < `NUM_INP);
		for(int i = 0; i < 5; i= i+1)
		begin
			if(col_a+i-v*max_STEPS >= row_a && col_a+i-v*max_STEPS <= row_a+`K-1)
				aT[row_a][col_a+i] <= vecA.sub(`K* row_a + col_a+i-row_a-v*max_STEPS+v*grid_SQ);		
		end
		
		if(col_a-(v*max_STEPS) > row_a+`K-1)
		begin
			row_a <= row_a + 1;
			col_a <= row_a + 1;
		end
		else
			col_a <= col_a + 5;
	endrule

/*****************************************************************************************************
/*	logic for filling the j-th col of bT:
/*		- for 0 <= r <= j-1, all entries are zeros
/*		- for j <= r <= j + k - 1, entries are B[r-j][j] from matrix B
/*		- for j + k <= r <= 2 * k - 2, all entries zeros
/*	where r denotes the row number in matrix bT 
/*****************************************************************************************************/
	rule init_B ((col_b <= `K - 1) && m<`NUM_INP);
		for(int i = 0; i < 5; i= i+1)
		begin
			if(row_b+i-m*max_STEPS >= col_b && row_b+i-m*max_STEPS <= col_b+`K-1)
			begin
				bT[row_b+i][col_b] <= vecB.sub(`K* (row_b+i-col_b-(m*max_STEPS)) + col_b+m*grid_SQ);		
			end
		end
		
		if(row_b-(m*max_STEPS) > col_b+`K-1)
		begin
			col_b <= col_b + 1;
			row_b <= col_b + 1;
		end
		else
			row_b <= row_b + 5;
	endrule
		
	rule set_A (row_a == `K);
		v <= v + 1;
		col_a <= col_a - `K + max_STEPS;
		row_a<=0;
	endrule

	rule set_B (col_b == `K);
		m <= m + 1;
		row_b <= row_b - `K +  max_STEPS;
		col_b <= 0;
	endrule
	
	// Method to query if pre-processing is done
	method Bool is_done if (m == `NUM_INP && v == `NUM_INP);
		return True;
	endmethod

	// Method to get the aT vector to be given as "x" input to first column of MAC grid at state t
	method Vector#(`K, int) get_aT (int t);
		Vector#(`K, int) aK; 
		for (Integer i = 0; i < `K; i = i + 1) 
			aK[i] = aT[i][t];
		return aK;
	endmethod

	// Method to get the bT vector to be given as "y" input to first row of MAC grid at state t
	method Vector#(`K, int) get_bT (int t);
		Vector#(`K, int) bK; 
		for (Integer i = 0; i < `K; i = i + 1) 
			bK[i] = bT[t][i];
		return bK;
	endmethod

  endmodule: mkREG

endpackage: get_input
