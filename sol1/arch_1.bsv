// Bluespec Project - Matrix - Matrix Multiplication using Systolic Arrays
//
// 			Authors
// Aman Goel 		  - EE11B087
// Dheeraj B 		  - EE11B090
// Chaitanya Peddawad - EE11B096
//
// This file describes the main module of Architecture 1 as specified in report. 
// It uses K x K number of MACs to compute out = A.B
// A propogates in horizontal direction, B in vertical direction through the MACs while out gets accumulated at the MACs

// Importing libraries and packages
import RegFile :: * ;
import Vector :: *;
import get_input :: *;
import mac :: *;

// Defining macros
`define K 5										// K is the dimension of the input square matrices
`define NUM_INP 4								// NUM_INP is the number of input matrices to multiply
												// i.e. out1 = A1.B1, out2 = A2.B2, ..., outNUM_INP = ANUM_INP.BNUM_INP

// This the main module. It constructs the MAC grid, take input from get_input module,
// Performs proper interfacing of inputs to each MAC and collects the output.
(* synthesize *)
  module mkArch_1 (Empty);

	// Defining variables used
	int max_STEPS = `K + `K + `K - 2;   		// 3K -2, this is the dimension of input matrix that enters the MAC grid.
	int max_MAC_ITR = max_STEPS + 1;			// Total number of MAC steps for every single matrix multiplication
	int max_ITR = max_MAC_ITR * `NUM_INP;		// Total number of MAC steps for all matrix multiplications to complete on a particular MAC

	// Declaring flags and counters
	Reg#(Bool) inpDone <- mkReg(False);			// Flag to indicate that input matrix is ready to be taken from get_input module
	Reg#(Bool) macDone <- mkReg(False);			// Flag to indicate that a single MAC operation on a MAC is done
	Reg#(Bool) putDone <- mkReg(False);			// Flag to indicate that new values are given to the MACs
	Reg#(int)  macNum  <- mkReg(0);				// Counter to indicate the state of MACs during a particular matrix multiplication 
	Reg#(int)  itrNum  <- mkReg(0);				// Counter to indicate the total number of MAC operations performed on a particular MAC

	// Instantiating modules
	REG_ifc 						  	regTop 	<- mkREG;										// Instantiating the regTop module specified in get_input		
	Vector#(`K, Vector#(`K, MAC_ifc))	mac 	<- replicateM(replicateM (mkMAC));				// Creating the K x K MAC grid
	Vector#(`K, Vector#(`K, Reg#(int))) x       <- replicateM(replicateM (mkReg(0)));			// Creating the K x K "x" input registers of the MAC grid
	Vector#(`K, Vector#(`K, Reg#(int))) y       <- replicateM(replicateM (mkReg(0)));			// Creating the K x K "y" input registers of the MAC grid


	// Following code used for opening the result file that will show the final outputs
	// This part is directly taken from BSV reference guide
	let fh <- mkReg(InvalidFile) ;
	let fmcd <- mkReg(InvalidFile) ;
	Reg#(int) outCount <- mkReg(0);
	
	// Rule to open the output file
	rule open_outfile (outCount == 0) ;
		// Open the file and check for proper opening
		// Using a multi-channel descriptor.
		String dumpFile = "results.txt" ;
		File lmcd <- $fopen( dumpFile ) ;
		if ( lmcd == InvalidFile )
		begin
			$display("cannot open %s", dumpFile );
			$finish(0);
		end
		lmcd = lmcd | stdout_mcd ;
		outCount <= 1;
		fmcd <= lmcd ;
	endrule

	// Rule to query if pre-processed input matrices are ready
	rule set_input (!inpDone);
		let done = regTop.is_done();						// is_done() from get_input file tells if pre-processing done or not
		if (done)											// If pre-processing is done, set inpDone and macDone for MAC operations to start
		begin 
			inpDone <= True;
			macDone <= True;
		end
	endrule

	// Rule to start the MAC units from mac file and set the number of operations at a MAC before MAC units get reset
	rule mac_start;
		// Iterating over all MAC units
		for (int i = 0; i < `K; i = i + 1)
		begin
			for (int j = 0; j < `K; j = j + 1)
			begin
				mac[i][j].put_start ();
				mac[i][j].put_numItr(max_MAC_ITR);
			end
		end
	endrule
    
	// descending urgency needed since both these rules can trigger at same time but preference should be given to mac_reset if can fire
	(* descending_urgency = "mac_reset, mac_fsm" *)

	// This rule resets all MAC units after every max_MAC_ITR steps to allow for next matrix multiplication
	rule mac_reset (inpDone && macDone && macNum == max_MAC_ITR);
		// Iterating over all MAC units
		for (int i = 0; i < `K; i = i + 1)
		begin
			for (int j = 0; j < `K; j = j + 1)
					mac[i][j].put_reset ();
		end
		macNum <= 0;							// Setting the state of MACs to initial default state
		itrNum <= itrNum - 1;					// Needed since number of MAC steps is (3K - 1) to allow for reset, while dimension of input matrices is (3K - 2) 
	endrule

	// This rule controls the flow of data to different MAC units based on the state of the MACs
	rule mac_fsm (inpDone && macDone);
	
		if (macNum < max_STEPS)										// i.e. still a particular matrices product is not ready
		begin
			// aT and bT are collected from get_input
			let aT = regTop.get_aT(itrNum);							// aT carries the column vector of size K that is given to the first column of MAC grid as "x" input
			let bT = regTop.get_bT(itrNum);							// bT carries the row vector of size K that is given to the first row of MAC grid as "y" input

			// Iterating over all MACs
			for (int i = 0; i < `K; i = i + 1)
			begin
				x[i][0] <= aT[i];									// Setting the "x" input of the first column of MAC grid 
				for (int j = 1; j < `K; j = j + 1)
				begin
					if (macNum == 0)								// Initial first state of MACs when data is just about to enter the MAC grid 
						x[i][j] <= 0;								// Resetting the "x" inputs of all columns except the first column
					else
						x[i][j] <= x[i][j-1];						// Not initial state, so take "x" input from the previous MAC 
				end
			end

			// Iterating over all MACs
			for (int i = 0; i < `K; i = i + 1)						// Setting the "y" input of the first row of MAC grid
			begin
				y[0][i] <= bT[i];									// Setting the "x" input of the first column of MAC grid 
				for (int j = 1; j < `K; j = j + 1)
				begin
					if (macNum == 0)								// Initial first state of MACs when data is just about to enter the MAC grid 
						y[j][i] <= 0;								// Resetting the "y" inputs of all rows except the first row
					else
						y[j][i] <= y[j-1][i];						// Not initial state, so take "y" input from the previous MAC above it 
				end
			end
		end

		else														// i.e. reached the last step of a matrix multiplication state
		begin

			// Iterating over all MACs
			for (int i = 0; i < `K; i = i + 1)
			begin
				for (int j = 1; j < `K; j = j + 1)
				begin
					x[i][j] <= 0;									// Setting all inputs of MACs as zero, 
					y[i][j] <= 0;									// since it is the last stage and is just waiting for last computation to complete at the MACs
				end
			end
		end

		itrNum <= itrNum + 1;										// Incrementing the total iteration counter
		macNum <= macNum + 1;										// Incrementing the state of the MACs
		macDone <= False;											// Setting macDone flag as false since new computation still not complete on the MACs
		putDone <= False;											// Setting putDone flag as false since new data has not been given to the MACs
	endrule

	// This rule puts the "x" & "y" data to the MACs 
	rule mac_put (inpDone && !macDone && !putDone);
		// iterating over all MACs
		for (int i = 0; i < `K; i = i + 1)
		begin
			for (int j = 0; j < `K; j = j + 1)
			begin
				mac[i][j].put_x (x[i][j]);							// Giving the "x" input to the MAC
				mac[i][j].put_y (y[i][j]);							// Giving the "y" input to the MAC
			end
		end	
		putDone <= True;											// Setting the putDone flag as put operation is completed
	endrule

	// This rule allows for checking if a matrix product is ready, and printing it in output file
	rule mac_work (inpDone && !macDone && putDone);
		if(itrNum == max_ITR)										// i.e. reached end of all computations, so end of program
			$finish (0);
		if(macNum == max_MAC_ITR)									// i.e. reached last stage of a particular matrix multiplication 
		begin
			// Iterating over all MACs
			for (int i = 0; i < `K; i = i + 1)
			begin
				for (int j = 0; j < `K; j = j + 1)
				begin
					let z = mac[i][j].get_out ();					// Querying get_out from the MAC unit which will fire only when a particular MAC operation is done
					$fwrite( fmcd , "%d\t", z);						// Writing the output to the output file
				end
				$fwrite( fmcd , "\n");
			end
			$fwrite( fmcd , "\n");
		end
		macDone <= True;											// Setting macDone flag since a particular MAC operation is done now 
	endrule

endmodule: mkArch_1

/////////////////////////////////   END    ///////////////////////////////
