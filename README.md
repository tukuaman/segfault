
									 "Bluespec Project - Matrix-Matrix Multiplication using Systolic Arrays"
															 **Team SegFault**
															 
													 			Authors
													 Chaitanya Peddawad - EE11B096 (crpiit@gmail.com)
													 Aman Goel 		    - EE11B087 (ee11b087@ee.iitm.ac.in)
													 Dheeraj B 		    - EE11B090 (ee11b090@ee.iitm.ac.in)
													 
This readme provides information how the project files and folder are arranged and instruction to run the codes.

* Refer to "SegFault_report.pdf" for the final report of the project containing consolidated synthesis results

* 4 different solutions have 2 folders per solution. One for bluespec simulation (bluesim) and other for Xilinx synthesis
( Top module is different and some of the modules are not synthesized since they were meant only for simulation, hence .bsv files for synthesis are slightly different
and are stored in separate folder 'sol<x>_xilinx/')

* Following instructions guide to run any solution (say solution 1) - simulate in bluespec, generate verilog files and then synthesize in xilinx

--> SIMULATING IN BLUESPEC !

	** Open bluespec and load the project from 'sol1/sol1.bspec'
	   NOTE: ( Top module is different and some of the modules are not synthesized since they were meant only for simulation, hence .bsv files for synthesis are slightly 				 different and are stored in separate folder 'sol1_xilinx/' instead of 'sol1/')
	** a.txt and b.txt files contain input matrices to be multiplied. Multiple multiplications are allowed- have to change the matrix size and number of inputs in each of 	      the .bsv files ( refer to " `define " params)
	

	--> CHECKING THE SIMULATION RESULTS	
	
		--> For solutions 1,2 and 3
		
			** Check the output in "results.txt" that gets generated. path- 'sol<x>/results.txt'
	

		--> For solution 4
		
			** The output file that first gets generated is "temp_results.txt". path -'sol4/temp_results.txt'
			** This output has jumbled order of elements in multiple matrices.
			** To rearrange the elements, please run python script "Descramble.py" to create a meaningful output file "results.txt" 
			   (paths- 'sol4/Descramble.py', 'sol4/temp_results.txt', 'sol4/results.txt')
	
	
--> GENERATING VERILOG FILES !
	
	** Open bluespec and load the project from 'sol1_xilinx/sol1_xilinx.bspec'
	** Compile and simulate to see that the verilog files have been generated in the folder 'sol1_xilinx/verilog/'
	** Now all these verilog files EXCEPT "mkTop.v" are added in xilinx project using the tab 'project-> add source'
	

--> SYNTHESIZING !

	** Open the project in xilinx from 'xilinx_project_files/SegFault.xise' - note that this project doesn't load any solution by default.
	** To load verilog files of solution <x>, use the 'add source' option from xilinx (project -> add sources) and load all the verilog files EXCEPT "mkTop.v" from the path 		   'sol<x>_xilinx/verilog/'
	** Click on top module .v file and synthesize to get synthesis results of solution <x>. (<x> = 1, 2, 3, 4)	 
	
	



